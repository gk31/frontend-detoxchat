import React, {FC, useEffect, useRef, useState} from "react";
import {User} from "./model/User";
import {useChat} from "./hooks/useChat";
import {ChatMessage} from "./model/ChatMessage";
import {MessageView} from "./MessageView";
import {ToxicMessageModes} from "./model/ToxicMessageModes";

type ChatProps = { users: User[], toxicMessageMode: ToxicMessageModes };
export const Chat: FC<ChatProps> = ({users, toxicMessageMode}) => {
    const {username, history, latest, sendMessage, warning, clearWarning, ignoreWarning} = useChat('ws://detox.chat:8001');
    const [received, setReceived] = useState<ChatMessage[]>([]);
    const [messageToSend, setMessageToSend] = useState('');
    const endOfList = useRef<HTMLSpanElement>(null);
    const submitMessage = () => {
        if (messageToSend) {
            sendMessage(JSON.stringify({type: 'send', text: messageToSend}));
            setMessageToSend('');
        }
    };
    useEffect(
        () => {
            setReceived(prev => latest ? [...prev, latest] : prev);
            endOfList.current?.scrollIntoView();
        },
        [latest]
    );
    useEffect(() => endOfList.current?.scrollIntoView(), [history]);
    const html = <>
        <div className="chat">
            {
                history?.concat(...received ?? [])
                    .map(({message, score}) => <MessageView toxicMessageMode={toxicMessageMode} score={score} message={message}/>)
            }
            <span ref={endOfList}/>
        </div>
        <form className="messageForm" action='#'>
            {warning === null
                ?
                    <>
                        <label className="messageInput">
                            <b className="user">{username}</b>
                            <input type="text" onChange={(e) => setMessageToSend(e.target.value)} value={messageToSend}/>
                        </label>
                        <label>
                            <button type='submit' onClick={submitMessage}>Send</button>
                        </label>
                    </>
                :
                    <>
                        <p className="warningMessage">Your message seems to be toxic!<br />
                        Toxic players will be excluded from exclusive rewards!<br />
                        Also players might lose access to the chat or account.</p>
                        <label className="messageInput">
                            <b className="user">{username}</b>
                        </label>
                        <button onClick={ignoreWarning}>send anyways</button>
                        <button onClick={clearWarning}>cancel message</button>
                    </>
                }
        </form>
    </>;
    return html;
};
