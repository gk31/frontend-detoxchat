import React, {FC, useState} from "react";
import {ToxicMessageModes} from "./model/ToxicMessageModes";

type MessageViewProps = { score: number, message: string, toxicMessageMode: ToxicMessageModes };
export const MessageView: FC<MessageViewProps> = ({score, message, toxicMessageMode}) => {
    const [masked, setMasked] = useState(true);
    const threshold = .5;
    return (
        toxicMessageMode === 'hide' && score > threshold
            ? <></>
            : <div className={'messageContainer ' + (score > .8 ? 'very ' : '') + (score > threshold ? 'toxic' : '')}>
                {score > threshold && toxicMessageMode !== 'show'
                    ? <>
                        {toxicMessageMode === 'mask'
                            ? <>
                                <div className="message">{masked ? '○'.repeat(message.length) : message}</div>
                                <button className="more" onClick={() => setMasked(!masked)}>{masked
                                    ? 'show'
                                    : 'hide'
                                }</button>
                            </>
                            : ''
                        }
                    </>
                    : <div className="message">{message}</div>
                }
            </div>
    );
};
