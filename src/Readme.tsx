import React from "react";

export function Readme() {
    return <pre className="readMe">
    <h3>readme</h3>
    <ul>
        <li>This is a chat room where toxicity is not allowed at all</li>
        <ul><li>though please write toxic stuff anyways to demonstrate the prototype is working as intended ;)</li></ul>
        <li>We use a google machine learning API - messages go through some google services</li>
        <li>this version only works for english</li>
    </ul>
  </pre>;
}