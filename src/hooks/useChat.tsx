import useWebSocket from "react-use-websocket";
import {useEffect, useState} from "react";
import {ChatMessage} from "../model/ChatMessage";

type Warning = ChatMessage;
type ChatDescriptor = {
    sendMessage: (message: string/*, keep?: boolean*/) => void,
    history: ChatMessage[] | null,
    username: string,
    latest: ChatMessage | null,
    warning: Warning | null,
    clearWarning: () => void,
    ignoreWarning: () => void
};
type UseChatType = (url: string) => ChatDescriptor;
export const useChat: UseChatType = (url) => {
    const {lastMessage, sendMessage} = useWebSocket(url);
    const [history, setHistory] = useState<ChatMessage[] | null>(null);
    const [username, setUsername] = useState<string>('');
    const [latest, setLatest] = useState<ChatMessage | null>(null);
    const [warning, setWarning] = useState<Warning | null>(null);
    const clearWarning = () => setWarning(null);
    const ignoreWarning = () => {
        sendMessage(JSON.stringify({type: 'ignore'}));
        setWarning(null);
    };
    useEffect(
        () => {
            if (history === null) {
                sendMessage(JSON.stringify({type: 'join'}));
                setHistory([]);
                window.addEventListener('beforeunload', () => sendMessage(JSON.stringify({type: 'leave'})));
            } else {
                const lastJsonMessage = JSON.parse(lastMessage?.data ?? 'null');
                switch (lastJsonMessage?.type) {
                    case 'register':
                        setUsername(lastJsonMessage.name);
                        break;
                    case 'history':
                        setHistory((lastJsonMessage.text as string[]).map(
                            (v, k) => ({message: v, score: lastJsonMessage.score[k]})
                        ));
                        break;
                    case 'send':
                        setLatest({message: lastJsonMessage.text, score: lastJsonMessage.score});
                        break;
                    case 'warn':
                        setWarning({message: lastJsonMessage.text, score: lastJsonMessage.score});
                        break;
                    default:
                        console.log(lastMessage?.data);
                        break;
                }
            }
        },
        [history, lastMessage, sendMessage]
    );
    return {username, history, latest, sendMessage, warning, clearWarning, ignoreWarning};
};
