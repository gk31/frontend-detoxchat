import React, {useState} from 'react';
import './App.css';
import {Readme} from "./Readme";
import {Settings} from "./Settings";
import {Chat} from "./Chat";
import {ToxicMessageModes} from "./model/ToxicMessageModes";

function App() {
    const [userAccounts, setUserAccounts] = useState([
        {id: 'e0bc1e24-fa52-4293-8fa7-2768039c8415', name: 'Jolly roger'},
        {id: '9e898562-f141-419f-b102-baa69dae076b', name: 'Tunas fall'},
        {id: '99adbf57-f8fe-41b7-b91d-a3884486c5e2', name: 'Damn yer'},
    ]);
    const [toxicMessageMode, setToxicMessageMode] = useState<ToxicMessageModes>('mask');
    return (
        <div className="main">
            <h1>Detox.Chat - Junction 2021</h1>
            <Chat users={userAccounts} toxicMessageMode={toxicMessageMode}/>
            <div className="settings">
                <h3>What to do with toxic messages?</h3>
                <p>
                    <label>
                        <input
                            type="radio"
                            name='mode'
                            value='show'
                            checked={toxicMessageMode === 'show'}
                            onClick={(e) => e.currentTarget.checked && setToxicMessageMode('show')}
                        />
                        show
                    </label>
                </p>
                <p>
                    <label>
                        <input
                            type="radio"
                            name='mode'
                            value='hide'
                            checked={toxicMessageMode === 'hide'}
                            onClick={(e) => e.currentTarget.checked && setToxicMessageMode('hide')}
                        />
                        hide
                    </label>
                </p>
                <p>
                    <label>
                        <input
                            type="radio"
                            name='mode'
                            value='mask'
                            checked={toxicMessageMode === 'mask'}
                            onClick={(e) => e.currentTarget.checked && setToxicMessageMode('mask')}
                        />
                        mask
                    </label>
                </p>
            </div>
            <Readme/>
        </div>
    );
}

export default App;
